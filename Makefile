# Copyright 1996 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for CDFS Driver

COMPONENT  = CDFSDriver
HEADER1    = CDROM
HEADER2    = CDErrors
ASMCHEADER1 = CDErrors
EXPORTS    = ${C_EXP_HDR}${SEP}${ASMCHEADER1}${SUFFIX_HEADER}
RESFSDIR   = ${RESDIR}${SEP}CDFSDriver${SEP}${TARGET}
ROM_SOURCE = Main.s

include AAsmModule

# Dynamic dependencies:
